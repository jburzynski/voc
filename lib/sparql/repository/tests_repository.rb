module Sparql::Repository
  class TestsRepository < Repository
    def initialize(sparql_client_class = Sparql::PlainClient)
      super(sparql_client_class)
    end

    def get_words(words)
      sparql_query = "SELECT ?s ?p ?o WHERE {?s ?p ?o. FILTER("
      for word in words
        sparql_query += " || " if sparql_query =~ /\?s \=/
        sparql_query += "?s = <#{word}>"
      end
      sparql_query += ")}"
      query = @sparql_client.query(sparql_query)

      result = {}
      query.each_solution do |solution|
        result[solution['s']] = {} unless result[solution['s']]
        if (RDF::Prefixes::RDFS+'label' == solution['p']['value'])
          result[solution['s']][solution['p']['value']] = {} unless result[solution['s']][solution['p']['value']]
          # custom keys for LOCALE and DE
          if (I18n.locale.to_s.upcase == solution['o']['xml:lang'])
            result[solution['s']][solution['p']['value']][I18n.locale.to_s.upcase] = solution['o']
          elsif ('DE' == solution['o']['xml:lang'])
            result[solution['s']][solution['p']['value']]['DE'] = solution['o']
          end
          #
        else
          result[solution['s']][solution['p']['value']] = solution['o']
        end
      end

      result
    end

  end
end