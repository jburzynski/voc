module Sparql::Repository
  class Repository
    attr_reader :sparql_client
    
    def initialize(sparql_client_class = Sparql::PlainClient)
      @sparql_client = sparql_client_class.new(Rails.configuration.sparql_endpoint_url)
    end
  end
end
