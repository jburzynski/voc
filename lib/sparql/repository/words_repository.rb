module Sparql::Repository
  class WordsRepository < Repository
    def initialize(sparql_client_class = Sparql::PlainClient)
      super(sparql_client_class)
    end

    def get_categories
      query = @sparql_client.query("
        PREFIX voc: <http://semvoc.net/voc#>
        PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
        SELECT ?s ?o WHERE {
          ?s a voc:Category. ?s rdfs:label ?o.
          FILTER(langMatches(lang(?o), \"#{I18n.locale}\"))
        } ORDER BY ?s"
      )
      result = {}
      query.each_solution do |solution|
        result[solution['s']] = solution['o']
      end
      result
    end

    def get_category_associations
      query = @sparql_client.query("
        PREFIX voc: <http://semvoc.net/voc#>
        SELECT ?s ?a ?v ?w WHERE {
          ?s voc:hasAssociation ?a. ?a voc:associationValue ?v. ?a voc:associationWeight ?w.
        }"
      )
      result = {}
      query.each_solution do |solution|
        result[solution['s']] = {} unless result[solution['s']]
        result[solution['s']][solution['a']] = {'v' => solution['v'], 'w' => solution['w']}
      end
      result
    end

    def get_words
      query = @sparql_client.query("
        PREFIX voc: <http://semvoc.net/voc#>
        PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
        SELECT ?s ?p ?o ?l WHERE {
          {?s ?p ?o. OPTIONAL {?o rdfs:label ?l}. ?c rdfs:subClassOf voc:Word. ?s a ?c.}
          UNION
          {?s ?p ?o. OPTIONAL {?o rdfs:label ?l}. ?s a voc:Word.}.
        } ORDER BY ?s
      ")
      result = {}
      query.each_solution do |solution|
        result[solution['s']] = {} unless result[solution['s']]
        result[solution['s']][solution['p']] = Array.new unless result[solution['s']][solution['p']]
        result[solution['s']][solution['p']].push('uri' == solution['o']['type'] ? {'o' => solution['o'], 'l' => solution['l']} : {'o' => solution['o']})
      end
      result
    end

    def get_words_display
      query = @sparql_client.query("
        PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
        select ?s ?o where {
          ?s rdfs:label ?o.
          FILTER(langMatches(lang(?o), \"de\")) .
        }"
      )
      result = {}
      query.each_solution do |solution|
        result[solution['s']] = solution['o']['value']
      end
      result
    end
    
  end
end