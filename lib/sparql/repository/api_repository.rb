module Sparql::Repository
  class ApiRepository < Repository
    def initialize(sparql_client_class = Sparql::PlainClient)
      super(sparql_client_class)
    end
    
    def get_advancement_levels(format)
      query = "
        PREFIX voc: <http://semvoc.net/voc#>
        SELECT * WHERE {?s ?p ?o. ?s a voc:AdvancementLevel} ORDER BY ?s
      "
      @sparql_client.response(query, format)
    end
    
    def get_all(format)
      query = "SELECT * WHERE {?s ?p ?o} ORDER BY ?s"
      @sparql_client.response(query, format)
    end
    
    def get_categories(format)
      query = "
        PREFIX voc: <http://semvoc.net/voc#>
        SELECT * WHERE {?s ?p ?o. ?s a voc:Category} ORDER BY ?s
      "
      @sparql_client.response(query, format)
    end
    
    def get_sparql(query, format)
      @sparql_client.response(query, format)
    end
    
    def get_words_adjectives(format)
      query = "
        PREFIX voc: <http://semvoc.net/voc#>
        SELECT * WHERE { ?s ?p ?o. ?s a voc:Adjective } ORDER BY ?s
      "
      @sparql_client.response(query, format)
    end
    
    def get_words_adverbs(format)
      query = "
        PREFIX voc: <http://semvoc.net/voc#>
        SELECT * WHERE { ?s ?p ?o. ?s a voc:Adverb } ORDER BY ?s
      "
      @sparql_client.response(query, format)
    end
    
    def get_words_all(format)
      query = "
        PREFIX voc: <http://semvoc.net/voc#>
        PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
        SELECT * WHERE {
          {?s ?p ?o. ?s a voc:Word}
          UNION
          {?s ?p ?o. ?c rdfs:subClassOf voc:Word. ?s a ?c}
        } ORDER BY ?s
      "
      @sparql_client.response(query, format)
    end
    
    def get_words_nouns(format)
      query = "
        PREFIX voc: <http://semvoc.net/voc#>
        SELECT * WHERE { ?s ?p ?o. ?s a voc:Noun } ORDER BY ?s
      "
      @sparql_client.response(query, format)
    end
    
    def get_words_other(format)
      query = "
        PREFIX voc: <http://semvoc.net/voc#>
        SELECT * WHERE { ?s ?p ?o. ?s a voc:Word } ORDER BY ?s
      "
      @sparql_client.response(query, format)
    end
    
    def get_words_verbs(format)
      query = "
        PREFIX voc: <http://semvoc.net/voc#>
        SELECT * WHERE { ?s ?p ?o. ?s a voc:Verb } ORDER BY ?s
      "
      @sparql_client.response(query, format)
    end
    
  end
end