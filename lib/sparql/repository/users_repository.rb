module Sparql::Repository
  class UsersRepository < Repository
    def initialize(sparql_client_class = Sparql::PlainClient)
      super(sparql_client_class)
    end

    def get_selections
      query = @sparql_client.query("
        PREFIX voc: <http://semvoc.net/voc#>
        PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
        SELECT * WHERE {
          {?s ?p ?o. ?s a voc:Word}
          UNION
          {?s ?p ?o. ?c rdfs:subClassOf voc:Word. ?s a ?c}
          UNION
          {?s ?p ?o. ?s a voc:Category}
        }");
      result = {}

      query.each_solution do |solution|
        result[solution['s']['value']] = {} unless result[solution['s']['value']]
        if (RDF::Prefixes::RDFS+'label' == solution['p']['value'])
          result[solution['s']['value']][solution['p']['value']] = {} unless result[solution['s']['value']][solution['p']['value']]
          # custom keys for LOCALE and DE
          if (I18n.locale.to_s.upcase == solution['o']['xml:lang'])
            result[solution['s']['value']][solution['p']['value']][I18n.locale.to_s.upcase] = solution['o']['value']
          elsif ('DE' == solution['o']['xml:lang'])
            result[solution['s']['value']][solution['p']['value']]['DE'] = solution['o']['value']
          end
          #
        else
          result[solution['s']['value']][solution['p']['value']] = solution['o']['value']
        end
      end

      result
    end

  end
end
