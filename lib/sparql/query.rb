module Sparql
  class Query
    def initialize(response)
      @result = JSON.parse(response)
    end
    
    def each_solution
      @result['results']['bindings'].each { |binding| yield binding }
    end
  end
end