module Sparql
  class PlainClient
    def initialize url
      @url = URI.parse(url)
    end
    
    def query(query)
      http = Net::HTTP.new(@url.host, @url.port)
      request = Net::HTTP::Post.new(@url.path, {'Accept' => 'application/sparql-results+json'})
      response = http.request(request, "query=#{query}")
      Query.new(response.body)
    end
    
    def response(query, format)
      if !formats[format]
        raise Sparql::Exceptions::InvalidFormat.new('invalid_format')
      end
      http = Net::HTTP.new(@url.host, @url.port)
      
      request = Net::HTTP::Get.new(@url.path+"?soft-limit=-1&query=#{query}", {'Accept' => formats[format]})
      http.request(request).body
    end
    
    def formats
      {
        'json' => 'application/sparql-results+json',
        'tsv' => 'text/tab-separated-values',
        'xml' => 'application/sparql-results+xml'
      }
    end
    
  end
end