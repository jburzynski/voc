module RDF
  class Prefixes
    RDFS = 'http://www.w3.org/2000/01/rdf-schema#'
    RDF = 'http://www.w3.org/1999/02/22-rdf-syntax-ns#'
    VOC = 'http://semvoc.net/voc#'
    SEMVOC = 'http://semvoc.net/semvoc#'
  end
end