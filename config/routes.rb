Voc::Application.routes.draw do
  
  devise_for :users
  get 'users/profile' => 'users#profile'
  match 'users/save_selections' => 'users#save_selections', via: [:post]
  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".

  # You can have the root of your site routed with "root"
  root 'words#index'

  # Example of regular route:
  #   get 'products/:id' => 'catalog#view'
  
  match 'words' => 'words#index', via: [:get, :post]
  match 'words/save_selection' => 'words#save_selection', via: [:post]
  
  match 'tests' => 'tests#index', via: [:post]
  match 'tests/autosave' => 'tests#autosave', via: [:post]
  get 'tests/autoload' => 'tests#autoload'
  match 'tests/test_state_clear' => 'tests#test_state_clear', via: [:post]
  match 'tests/save_score' => 'tests#save_score', via: [:post]
  
  get 'api' => 'api#index'
  get 'api/sparql/:format' => 'api#sparql'
  get 'api/:selector/:format' => 'api#predefined'

  # Example of named route that can be invoked with purchase_url(id: product.id)
  #   get 'products/:id/purchase' => 'catalog#purchase', as: :purchase

  # Example resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Example resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Example resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Example resource route with more complex sub-resources:
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', on: :collection
  #     end
  #   end
  
  # Example resource route with concerns:
  #   concern :toggleable do
  #     post 'toggle'
  #   end
  #   resources :posts, concerns: :toggleable
  #   resources :photos, concerns: :toggleable

  # Example resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end
end
