class CreateCategorySelections < ActiveRecord::Migration
  def change
    create_table :category_selections do |t|
      t.belongs_to :selection
      
      t.string :category

      t.timestamps
    end
  end
end
