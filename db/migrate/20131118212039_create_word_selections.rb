class CreateWordSelections < ActiveRecord::Migration
  def change
    create_table :word_selections do |t|
      t.belongs_to :selection
      
      t.string :word

      t.timestamps
    end
  end
end
