class CreateTestEntries < ActiveRecord::Migration
  def change
    create_table :test_entries do |t|
      t.belongs_to :test_state
      
      t.string :word
      t.string :article
      t.string :german
      t.boolean :countable
      t.string :plural_suffix
      t.boolean :umlaut
      t.string :praeteritum
      t.string :perfekt

      t.timestamps
    end
  end
end
