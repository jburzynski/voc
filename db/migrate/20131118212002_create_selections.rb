class CreateSelections < ActiveRecord::Migration
  def change
    create_table :selections do |t|
      t.belongs_to :user
      
      t.string :name

      t.timestamps
    end
  end
end
