class CreateTestStates < ActiveRecord::Migration
  def change
    create_table :test_states do |t|
      t.belongs_to :user
      
      t.timestamps
    end
  end
end
