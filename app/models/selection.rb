class Selection < ActiveRecord::Base
  belongs_to :user
  
  has_many :category_selections, dependent: :destroy
  has_many :word_selections, dependent: :destroy
end
