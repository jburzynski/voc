class TestState < ActiveRecord::Base
  belongs_to :user
  has_many :test_entries, dependent: :destroy
end