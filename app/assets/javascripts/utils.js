var post_to_url = function(url, params) {
    var form = document.createElement('form');
    form.setAttribute('method', 'post');
    form.setAttribute('action', url);

    for (var key in params) {
        if (params.hasOwnProperty(key)) {
            if (typeof params[key] === 'Array') {
                params[key].every(function(element) {
                    var hiddenField = document.createElement('input');
                    hiddenField.setAttribute('type', 'hidden');
                    hiddenField.setAttribute('name', key + '[]');
                    hiddenField.setAttribute('value', element);

                    form.appendChild(hiddenField);
                });
            } else {
                var hiddenField = document.createElement('input');
                hiddenField.setAttribute('type', 'hidden');
                hiddenField.setAttribute('name', key);
                hiddenField.setAttribute('value', params[key]);

                form.appendChild(hiddenField);
            }
         }
    }

    document.body.appendChild(form);
    form.submit();
};