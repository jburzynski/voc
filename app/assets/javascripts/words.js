var weightedColor = function(weight) {
    if (weight >= 0 && weight <= 1) {
        var offset = 40;
        var max = 255;
        var value = max - parseInt(offset + weight * (max - offset));
        var valueHex = value.toString(16);
        return '#' + valueHex + valueHex + valueHex;
    }
    return false;
};