class WordsController < ApplicationController
  protect_from_forgery except: [:index]
  before_filter :authenticate_user!, only: [:save_selection]
  
  def index
    # initialize repository
    repository = Sparql::Repository::WordsRepository.new
    
    # get kb data
    @words_display = repository.get_words_display
    @words = repository.get_words
    @categories = repository.get_categories
    @category_associations = repository.get_category_associations
    
    # assign words to columns
    @col_count = 3
    col_size = @words_display.length / @col_count
    col_size, @col_count = 1, @words_display.length if 0 == col_size
    @col = Array.new
    
    @col_count.times do |i|
      @col[i] = Hash.new
    end
    
    i = 0
    @words_display.each do |key, val|
      col_index = i / col_size;
      col_index -= 1 if col_index > @col_count - 1;
      @col[col_index][key] = val
      i += 1
    end
    
    # parse selections
    if params['selections']
      @selections = JSON.parse(params['selections'])
    end
  end
  
  def save_selection
    raise Exception.new('No words or categories selected.') unless params['word_selections'] || params['category_selections']
    
    selection = Selection.new(name: 'selection', user: current_user)
    selection.save
    if params['word_selections']
      params['word_selections'].each {|word_selection| selection.word_selections.create(word: word_selection[1]['word'])}
    end
    if params['category_selections']
      params['category_selections'].each {|category_selection| selection.category_selections.create(category: category_selection[1]['category'])}
    end
    
    render json: {'status' => 'ok'}
  end
end