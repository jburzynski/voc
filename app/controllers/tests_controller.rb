class TestsController < ApplicationController
  protect_from_forgery except: [:index, :autosave, :save_score]
  before_filter :authenticate_user!, except: [:index]
  
  def index
    raise Exception.new('No words selected.') unless params[:words]
    
    repository = Sparql::Repository::TestsRepository.new
    
    words = params[:words].split(/,/)
    @words = repository.get_words(words)
    
    raise Exception.new('Too many words selected. Maximum is 20.') if @words.length > 20
    
    current_user.test_state.destroy if current_user && current_user.test_state
  end
  
  def autoload
    test_state = current_user.test_state
    raise Exception.new('No test state found for a given user.') unless test_state
    
    repository = Sparql::Repository::TestsRepository.new
    
    words = test_state.test_entries.map {|e| e.word}
    @words = repository.get_words(words)
    @test_entries = {}
    test_state.test_entries.each {|e| @test_entries[e.word] = e}
    
    render :index
  end
  
  def autosave
    test_state = current_user.test_state || TestState.new
    unless current_user.test_state
      test_state.user = current_user
      test_state.save
    end
    
    test_entries = test_state.test_entries
    params[:test_entries].each do |key, value|
      test_entry = test_entries.select {|e| e.word == value['word']}
      if test_entry.empty?
        test_entry = TestEntry.new(test_entry_params(key))
        test_entry.test_state = test_state
        test_entry.save
      else
        test_entry.first.update(test_entry_params(key))
      end
    end
    
    render json: {'status' => 'ok'}
  end
  
  def save_score
    test_result = TestResult.new(test_result_params)
    test_result.user = current_user
    test_result.save
    
    redirect_to users_profile_path, notice: I18n.t('tests.message.save_success')
  end
  
  def test_state_clear
    current_user.test_state.destroy if current_user.test_state
    
    render json: {'status' => 'ok'}
  end
  
  private
  
  def test_entry_params(key)
    params[:test_entries].require(key).permit(:word, :article, :german, :countable, :plural_suffix, :umlaut, :praeteritum, :perfekt)
  end
  
  def test_result_params
    params.require(:test_result).permit(:score, :total)
  end
end