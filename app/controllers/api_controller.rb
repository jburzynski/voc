class ApiController < ApplicationController
  def index
    
  end
  
  def predefined
    if !params[:selector] || !params[:format]
      render text: I18n.t('api.message.no_selector_or_format')
      return
    end
    
    repository = Sparql::Repository::ApiRepository.new
    
    begin
      case params[:selector]
      when 'advancement_levels'
        result = repository.get_advancement_levels(params[:format])
      when 'all'
        result = repository.get_all(params[:format])
      when 'categories'
        result = repository.get_categories(params[:format])
      when 'words_adjectives'
        result = repository.get_words_adjectives(params[:format])
      when 'words_adverbs'
        result = repository.get_words_adverbs(params[:format])
      when 'words_all', 'words'
        result = repository.get_words_all(params[:format])
      when 'words_nouns'
        result = repository.get_words_nouns(params[:format])
      when 'words_other'
        result = repository.get_words_other(params[:format])
      when 'words_verbs'
        result = repository.get_words_verbs(params[:format])
      else
        render text: I18n.t('api.message.bad_selector')
        return
      end
    rescue Sparql::Exceptions::InvalidFormat => e
      render text: I18n.t('api.message.' + e.message)
      return
    end
    
    content_type = repository.sparql_client.formats[params[:format]]
    render text: result, content_type: content_type
  end
  
  def sparql
    if !params[:query]
      render text: I18n.t('api.message.no_query')
      return
    end
    
    repository = Sparql::Repository::ApiRepository.new
    
    begin
      result = repository.get_sparql(params[:query], params[:format])
      content_type = repository.sparql_client.formats[params[:format]]
      render text: result, content_type: content_type
    rescue Sparql::Exceptions::InvalidFormat => e
      render text: I18n.t('api.message.' + e.message)
    end
  end
  
end