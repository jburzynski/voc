class UsersController < ApplicationController
  before_filter :authenticate_user!
  
  def profile
    repository = Sparql::Repository::UsersRepository.new
    
    @test_results = current_user.test_results.order(created_at: :desc)
    @selections = current_user.selections.order(created_at: :desc)
    @triples = repository.get_selections
  end
  
  def save_selections
    ActiveRecord::Base.transaction do
      params['selections'].each do |key, val|
        selection = Selection.find_by_id(Integer(val['id']))
        if selection
          if val['_destroy']
            selection.destroy 
          else
            selection.name = val['name']
            selection.save
          end
        end
      end
    end
    
    render json: {'status' => 'ok'}
  end

end