require 'minitest/autorun'

class QueryTest < MiniTest::Unit::TestCase
  
  def response
    '{
      "head":{"vars":["s","p","o"]},
      "results": {
        "bindings":[{
          "s":{"type":"uri","value":"http://semvoc.net/voc#IntermediateAdvancementLevel"},
          "p":{"type":"uri","value":"http://semvoc.net/voc#directlyLowerThan"},
          "o":{"type":"uri","value":"http://semvoc.net/voc#AdvancedAdvancementLevel"}
        }]
      }
    }'
  end
  
  def setup
    @query = Sparql::Query.new(response)
  end
  
  def test_each_solution
    expected = [{
      "s" => {"type" => "uri", "value" => "http://semvoc.net/voc#IntermediateAdvancementLevel"},
      "p" => {"type" => "uri", "value" => "http://semvoc.net/voc#directlyLowerThan"},
      "o" => {"type" => "uri", "value" => "http://semvoc.net/voc#AdvancedAdvancementLevel"}
    }]
    
    actual = []
    @query.each_solution do |solution|
      actual.push(solution)
    end
    
    (0..(expected.length - 1)).each do |i|
      assert_equal(expected[i], actual[i])
    end
  end
  
end