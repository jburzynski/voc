\contentsline {chapter}{\numberline {1}Wst\IeC {\k e}p}{3}{chapter.1}
\contentsline {section}{\numberline {1.1}Cel projektu}{3}{section.1.1}
\contentsline {section}{\numberline {1.2}Opis projektu}{3}{section.1.2}
\contentsline {chapter}{\numberline {2}Wykorzystane technologie}{4}{chapter.2}
\contentsline {section}{\numberline {2.1}RDF, OWL}{4}{section.2.1}
\contentsline {section}{\numberline {2.2}REST}{4}{section.2.2}
\contentsline {section}{\numberline {2.3}JSON}{4}{section.2.3}
\contentsline {section}{\numberline {2.4}JavaScript}{4}{section.2.4}
\contentsline {subsection}{\numberline {2.4.1}jQuery}{5}{subsection.2.4.1}
\contentsline {subsection}{\numberline {2.4.2}Knockout.js}{5}{subsection.2.4.2}
\contentsline {subsection}{\numberline {2.4.3}Inne biblioteki JS}{5}{subsection.2.4.3}
\contentsline {section}{\numberline {2.5}Bootstrap}{5}{section.2.5}
\contentsline {section}{\numberline {2.6}SASS}{5}{section.2.6}
\contentsline {section}{\numberline {2.7}Ruby}{6}{section.2.7}
\contentsline {section}{\numberline {2.8}Rails}{6}{section.2.8}
\contentsline {subsection}{\numberline {2.8.1}Devise}{6}{subsection.2.8.1}
\contentsline {subsection}{\numberline {2.8.2}CanCan}{6}{subsection.2.8.2}
\contentsline {subsection}{\numberline {2.8.3}Sparql}{6}{subsection.2.8.3}
\contentsline {subsection}{\numberline {2.8.4}GIT}{7}{subsection.2.8.4}
\contentsline {chapter}{\numberline {3}U\IeC {\.z}yte oprogramowanie}{8}{chapter.3}
\contentsline {section}{\numberline {3.1}Apache}{8}{section.3.1}
\contentsline {section}{\numberline {3.2}4store}{8}{section.3.2}
\contentsline {section}{\numberline {3.3}MySQL}{8}{section.3.3}
\contentsline {section}{\numberline {3.4}Ubuntu Linux}{8}{section.3.4}
\contentsline {section}{\numberline {3.5}VMWare Player}{9}{section.3.5}
\contentsline {section}{\numberline {3.6}Netbeans}{9}{section.3.6}
\contentsline {section}{\numberline {3.7}TopBraid Composer}{9}{section.3.7}
\contentsline {section}{\numberline {3.8}Google Chrome}{9}{section.3.8}
\contentsline {section}{\numberline {3.9}TeXstudio}{10}{section.3.9}
\contentsline {section}{\numberline {3.10}Gephi}{10}{section.3.10}
\contentsline {section}{\numberline {3.11}Visual Paradigm}{10}{section.3.11}
\contentsline {section}{\numberline {3.12}DIA}{10}{section.3.12}
\contentsline {chapter}{\numberline {4}Opis projektu}{11}{chapter.4}
\contentsline {section}{\numberline {4.1}Przypadki u\IeC {\.z}ycia}{11}{section.4.1}
\contentsline {section}{\numberline {4.2}Komponenty systemu}{13}{section.4.2}
\contentsline {section}{\numberline {4.3}Opis sieci semantycznej}{14}{section.4.3}
\contentsline {subsection}{\numberline {4.3.1}Ontologia}{14}{subsection.4.3.1}
\contentsline {subsection}{\numberline {4.3.2}Okre\IeC {\'s}lanie podobie\IeC {\'n}stwa s\IeC {\l }\IeC {\'o}wek}{15}{subsection.4.3.2}
\contentsline {section}{\numberline {4.4}API systemu}{16}{section.4.4}
\contentsline {subsection}{\numberline {4.4.1}Zapytania \textit {SPARQL}}{17}{subsection.4.4.1}
\contentsline {subsection}{\numberline {4.4.2}Predefiniowane komendy}{17}{subsection.4.4.2}
\contentsline {chapter}{\numberline {5}Szczeg\IeC {\'o}\IeC {\l }y implementacji}{18}{chapter.5}
\contentsline {section}{\numberline {5.1}Operacje przeprowadzane w systemie}{18}{section.5.1}
\contentsline {subsection}{\numberline {5.1.1}Pobieranie danych z bazy wiedzy}{18}{subsection.5.1.1}
\contentsline {subsection}{\numberline {5.1.2}Przeprowadzanie testu ze s\IeC {\l }\IeC {\'o}wek}{19}{subsection.5.1.2}
\contentsline {subsection}{\numberline {5.1.3}Pobieranie danych z bazy wiedzy na potrzeby API systemu}{20}{subsection.5.1.3}
\contentsline {section}{\numberline {5.2}Lista s\IeC {\l }\IeC {\'o}wek}{21}{section.5.2}
